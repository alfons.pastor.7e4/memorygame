package cat.itb.CallOfAesir

//a bit better than passing strings around
enum class FlipEvent {
    FAIL, MATCH, CONTINUE
}