package cat.itb.CallOfAesir

import android.os.Bundle
import android.os.CountDownTimer
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider

class GameActivity : AppCompatActivity() {
    lateinit var movesText: TextView
    lateinit var viewmodel: GameViewModel
    lateinit var runeImageViews: ArrayList<ImageView>
    lateinit var gameMode: String
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MemoryGame)
        super.onCreate(savedInstanceState)
        viewmodel = ViewModelProvider(this)[GameViewModel::class.java]
        viewmodel.initialize(intent.getStringExtra("game_mode").toString())
        gameMode = viewmodel.gameMode
        if (gameMode == "NORMAL") {
            setContentView(R.layout.activity_game_normal)
        } else if (gameMode == "HARD") {
            setContentView(R.layout.activity_game_hard)
        }
        movesText = findViewById(R.id.moves_text)
        runeImageViews = arrayListOf()
        for (i in 0 until viewmodel.runeAmount) {
            runeImageViews.add(
                findViewById(
                    resources.getIdentifier(
                        "imageView${i + 1}",
                        "id",
                        packageName
                    )
                )
            )
        }
        for (i in 0 until viewmodel.runeAmount) {
            runeImageViews[i].setOnClickListener {
                val state: FlipEvent = viewmodel.flipRune(i)
                updateUI()
                when (state) {
                    FlipEvent.FAIL -> {
                        val timer = object : CountDownTimer(1000, 100) {
                            override fun onTick(millisUntilFinished: Long) {}
                            override fun onFinish() {
                                viewmodel.resetRunes()
                                updateUI()
                            }
                        }
                        timer.start()
                    }
                    FlipEvent.MATCH -> {
                        val timer = object : CountDownTimer(500, 100) {
                            override fun onTick(millisUntilFinished: Long) {}
                            override fun onFinish() {
                                viewmodel.deactivateRunes()
                                updateUI()
                                if (viewmodel.isBoardSolved()) {
                                    intent.setClass(this@GameActivity, ResultActivity::class.java)
                                    intent.putExtra("score", viewmodel.score())
                                    startActivity(intent)
                                }
                            }
                        }
                        timer.start()
                    }
                    FlipEvent.CONTINUE -> {
                        updateUI()
                    }
                }
            }
        }
        updateUI()
    }

    fun updateUI() {
        movesText.text = viewmodel.moves.toString()
        for (i in 0 until viewmodel.runeAmount) {
            runeImageViews[i].setImageResource(
                resources.getIdentifier(
                    viewmodel.runes[i].getImage(), "drawable",
                    packageName
                )
            )
        }
    }

}