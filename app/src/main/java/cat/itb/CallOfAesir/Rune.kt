package cat.itb.CallOfAesir

class Rune(var hidden: Boolean, var imageIdentifier: String, var solved: Boolean) {
    fun flip() {
        hidden = !hidden
    }

    fun getImage(): String {
        return when {
            solved -> {
                "solved"
            }
            hidden -> {
                "hidden"
            }
            else -> {
                imageIdentifier
            }
        }
    }
}