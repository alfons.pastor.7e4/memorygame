package cat.itb.CallOfAesir

import androidx.lifecycle.ViewModel
import kotlin.properties.Delegates

class GameViewModel : ViewModel() {
    var runes: ArrayList<Rune> = arrayListOf()
    var distinctImages = 24
    var runeAmount by Delegates.notNull<Int>()
    var matchingNumber by Delegates.notNull<Int>()
    var moves = 0
    var locked = false
    var initialized = false
    lateinit var gameMode: String

    fun initialize(gameMode: String) {
        if (!initialized) {
            initialized = true
            this.gameMode = gameMode
            when (gameMode) {
                "NORMAL" -> {
                    runeAmount = 6; matchingNumber = 2
                }
                "HARD" -> {
                    runeAmount = 8; matchingNumber = 2
                }
            }
            val imageIdentifiers: ArrayList<String> = arrayListOf()
            val chosenImages: ArrayList<String> = arrayListOf()
            for (i in 0 until distinctImages) {
                imageIdentifiers.add("rune_${i}")
            }
            imageIdentifiers.shuffle()
            for (i in 0 until runeAmount / matchingNumber) {
                for (j in 0 until matchingNumber) {
                    chosenImages.add(imageIdentifiers[i])
                }
            }
            chosenImages.shuffle()
            for (i in 0 until runeAmount) {
                runes.add(Rune(true, chosenImages[i], false))
            }
        }
    }

    fun matchingRunes(): Int {
        var matching = 0
        for (rune in runes) {
            for (other in runes) {
                if ((other != rune) && (rune.imageIdentifier == other.imageIdentifier) && !rune.hidden && !other.hidden) {
                    matching++
                    break
                }
            }
        }
        return matching
    }

    fun isBoardSolved(): Boolean {
        runes.forEach {
            if (!it.solved) {
                return false
            }
        }
        return true
    }

    fun deactivateRunes() {
        runes.forEach {
            if (!it.hidden) {
                it.hidden = true
                it.solved = true
            }
        }
        locked = false
    }

    fun resetRunes() {
        for (rune in runes) {
            rune.hidden = true
        }
        locked = false
    }

    fun flippedRunes(): Int {
        var n = 0
        runes.forEach {
            if (!it.hidden) {
                n++
            }
        }
        return n
    }

    fun score(): Int {
        return (1000f * runeAmount / moves).toInt()
    }

    fun flipRune(i: Int): FlipEvent {
        if (runes[i].hidden && !runes[i].solved && !locked) {
            moves++
            runes[i].flip()
            if (flippedRunes() > 1 && flippedRunes() != matchingRunes()) {
                locked = true
                return FlipEvent.FAIL
            } else if (matchingRunes() == matchingNumber) {
                locked = true
                return FlipEvent.MATCH
            }
        }
        return FlipEvent.CONTINUE
    }
}