package cat.itb.CallOfAesir

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MenuActivity : AppCompatActivity() {
    private lateinit var playButton: Button
    private lateinit var helpButton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_MemoryGame)
        setContentView(R.layout.activity_menu)
        val spinner = findViewById<Spinner>(R.id.spinner)
        ArrayAdapter.createFromResource(
            this,
            R.array.game_modes,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }

        playButton = findViewById(R.id.playbutton)
        playButton.setOnClickListener {
            val intent = Intent(this, GameActivity::class.java)
            intent.putExtra("game_mode", spinner.selectedItem.toString())
            startActivity(intent)
        }
        helpButton = findViewById(R.id.help_button)
        helpButton.setOnClickListener {
            val dialog = Dialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.dialog_help)
            val body: TextView = dialog.findViewById(R.id.help_text)
            body.text = getString(R.string.help)
            val okButton: Button = dialog.findViewById(R.id.ok_button)
            okButton.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }
    }
}