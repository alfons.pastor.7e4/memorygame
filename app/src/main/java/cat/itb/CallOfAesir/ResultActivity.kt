package cat.itb.CallOfAesir

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class ResultActivity : AppCompatActivity() {
    lateinit var scoreText: TextView
    lateinit var playAgainButton: Button
    lateinit var menuButton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MemoryGame)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        scoreText = findViewById(R.id.score_text)
        scoreText.text = intent.getIntExtra("score", 0).toString()
        playAgainButton = findViewById(R.id.play_again_button)
        playAgainButton.setOnClickListener {
            intent.setClass(this, GameActivity::class.java)
            startActivity(intent)
        }
        menuButton = findViewById(R.id.menu_button)
        menuButton.setOnClickListener {
            intent.setClass(this, MenuActivity::class.java)
            startActivity(intent)
        }
    }
}